package hrytsenko.oss;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import hrytsenko.oss.ext.CsvWriter;

public class App implements WebApplicationInitializer {

    @Configuration
    @EnableWebMvc
    @ComponentScan
    public static class Config extends WebMvcConfigurerAdapter {

        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(new MappingJackson2HttpMessageConverter());
            converters.add(new CsvWriter());
        }

    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext app = new AnnotationConfigWebApplicationContext();
        app.register(App.Config.class);

        DispatcherServlet servlet = new DispatcherServlet(app);
        ServletRegistration.Dynamic reg = servletContext.addServlet(App.class.getName(), servlet);
        reg.addMapping("/");
        reg.setLoadOnStartup(0);
    }

}
