package hrytsenko.oss.ext;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.ChainrFactory;

import hrytsenko.oss.ext.helper.JoltHelper;
import lombok.AllArgsConstructor;

public class JoltTransformer implements ClientHttpRequestInterceptor {

    private Chainr jolt;

    public JoltTransformer(String spec) {
        jolt = ChainrFactory.fromClassPath(spec);
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        try (ClientHttpResponse response = execution.execute(request, body)) {
            String inputJson = IOUtils.toString(response.getBody(), StandardCharsets.UTF_8);
            String outputJson = JoltHelper.transform(jolt, inputJson);
            return new ClientHttpResponsetWrapper(response, outputJson.getBytes(StandardCharsets.UTF_8));
        }
    }

    @AllArgsConstructor
    private static class ClientHttpResponsetWrapper implements ClientHttpResponse {

        private ClientHttpResponse response;
        private byte[] body;

        @Override
        public HttpHeaders getHeaders() {
            return response.getHeaders();
        }

        @Override
        public InputStream getBody() throws IOException {
            return new ByteArrayInputStream(body);
        }

        @Override
        public String getStatusText() throws IOException {
            return response.getStatusText();
        }

        @Override
        public int getRawStatusCode() throws IOException {
            return response.getRawStatusCode();
        }

        @Override
        public HttpStatus getStatusCode() throws IOException {
            return response.getStatusCode();
        }

        @Override
        public void close() {
            // Closing is not required.
        }
    }

}
