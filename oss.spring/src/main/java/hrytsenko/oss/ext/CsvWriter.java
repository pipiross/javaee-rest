package hrytsenko.oss.ext;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.GenericHttpMessageConverter;

import hrytsenko.oss.ext.helper.CsvHelper;

public class CsvWriter implements GenericHttpMessageConverter<Object> {

    public static final MediaType TEXT_CSV = MediaType.valueOf("text/csv");

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return Arrays.asList(TEXT_CSV);
    }

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return canRead(clazz, null, mediaType);
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        return false;
    }

    @Override
    public Object read(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException {
        return read(clazz, null, inputMessage);
    }

    @Override
    public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return canWrite(clazz, clazz, mediaType);
    }

    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        return Objects.equals(TEXT_CSV, mediaType) && CsvHelper.getActualEntity(type).isPresent();
    }

    @Override
    public void write(Object entity, MediaType contentType, HttpOutputMessage outputMessage) throws IOException {
        write(entity, entity.getClass(), contentType, outputMessage);
    }

    @Override
    public void write(Object entity, Type type, MediaType contentType, HttpOutputMessage outputMessage)
            throws IOException {
        CsvHelper.write(entity, type, outputMessage.getBody());
    }

}
