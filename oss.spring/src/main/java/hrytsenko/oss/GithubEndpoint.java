package hrytsenko.oss;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import hrytsenko.oss.ext.JoltTransformer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GithubEndpoint {

    private static final String REPOS_API = "https://api.github.com/users/{username}/repos";
    private static final String REPOS_SPEC = "/github/repositories_spec.json";

    public List<Project> findProjects(String username) {
        log.info("Find projects for {}.", username);

        RestTemplate client = new RestTemplate();
        client.setInterceptors(Arrays.asList(new JoltTransformer(REPOS_SPEC)));

        return client.exchange(new RequestEntity<>(headers(), HttpMethod.GET, repositoriesFor(username)),
                new ParameterizedTypeReference<List<Project>>() {
                }).getBody();
    }

    private HttpHeaders headers() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.USER_AGENT, GithubEndpoint.class.getName());
        return headers;
    }

    private URI repositoriesFor(String username) {
        return UriComponentsBuilder.fromHttpUrl(REPOS_API).buildAndExpand(username).toUri();
    }

}
