package hrytsenko.oss;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("github")
public interface GithubResource {

    @GET
    @Path("{username}/projects")
    @Produces({ MediaType.APPLICATION_JSON, "text/csv" })
    List<Project> findProjects(@PathParam("username") String username);

}
