package hrytsenko.oss;

import org.apache.camel.builder.RouteBuilder;

public class DispatchRequest extends RouteBuilder {

    private static final String ENDPOINT = "cxfrs:bean:ossGithub?bindingStyle=SimpleConsumer";

    @Override
    public void configure() {
        from(ENDPOINT).toD("direct:${header.operationName}").end();
    }

}
