package hrytsenko.oss;

import javax.ws.rs.HttpMethod;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.component.jolt.JoltConstants;

public class FindProjects extends RouteBuilder {

    private static final String URI = "direct:findProjects";

    private static final String GITHUB = "cxfrs:bean:github";

    private static final String JOLT = "jolt:dummy?contentCache=true&inputType=JsonString&outputType=JsonString";

    private static final String REPOS_URI = "/users/${header.username}/repos";
    private static final String REPOS_SPEC = "/github/repositories_spec.json";

    @Override
    public void configure() {
        from(URI).setHeader(Exchange.HTTP_METHOD).simple(HttpMethod.GET).setHeader(Exchange.HTTP_PATH).simple(REPOS_URI)
                .removeHeaders("*", Exchange.HTTP_METHOD, Exchange.HTTP_PATH).to(GITHUB)
                .setHeader(JoltConstants.JOLT_RESOURCE_URI).simple(REPOS_SPEC).to(JOLT)
                .unmarshal(new ListJacksonDataFormat(Project.class));
    }

}
