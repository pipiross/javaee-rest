call %SERVICEMIX_HOME%\bin\client.bat -h 127.0.0.1 -u smx -- feature:install camel-jackson
call %SERVICEMIX_HOME%\bin\client.bat -h 127.0.0.1 -u smx -- feature:install camel-jolt
call %SERVICEMIX_HOME%\bin\client.bat -h 127.0.0.1 -u smx -- bundle:install wrap:mvn:com.fasterxml.jackson.dataformat/jackson-dataformat-csv/2.6.3
call %SERVICEMIX_HOME%\bin\client.bat -h 127.0.0.1 -u smx -- bundle:install wrap:mvn:com.fasterxml.jackson.jaxrs/jackson-jaxrs-base/2.6.3
call %SERVICEMIX_HOME%\bin\client.bat -h 127.0.0.1 -u smx -- bundle:install wrap:mvn:com.fasterxml.jackson.jaxrs/jackson-jaxrs-json-provider/2.6.3
