# Summary
Example of the RESTful web service which is built using different technologies.

This service receives the list of open-source projects from [Github] and returns it in JSON or CSV.

| Module       | Technology | Server       |
|--------------|------------|--------------|
| `oss.javaee` | [Java EE]  | [WildFly]    |
| `oss.spring` | [Spring]   | [Undertow]   |
| `oss.camel`  | [Camel]    | [ServiceMix] |

  [Java EE]: <http://docs.oracle.com/javaee/>
  [WildFly]: <http://wildfly.org/>
  [Spring]: <http://spring.io/>
  [Undertow]: <http://undertow.io/>
  [Camel]: <http://camel.apache.org/>
  [ServiceMix]: <http://servicemix.apache.org/>

  [Github]: <http://github.com/>
