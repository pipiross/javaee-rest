package hrytsenko.oss;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Project {

    private String owner;
    private String url;
    private String name;
    private String description;

}
