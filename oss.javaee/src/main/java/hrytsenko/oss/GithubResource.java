package hrytsenko.oss;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lombok.Setter;

@Path("github")
public class GithubResource {

    @Setter(onMethod = @__(@Inject))
    private GithubEndpoint endpoint;

    @GET
    @Path("{username}/projects")
    @Produces({ MediaType.APPLICATION_JSON, "text/csv" })
    public List<Project> findProjects(@PathParam("username") String username) {
        return endpoint.findProjects(username);
    }

}
